Rails.application.routes.draw do
  get 'blog_posts/show'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :blog_posts, only: %i[show]
end
