require 'pact/consumer/rspec'

Pact.service_consumer 'BlogService' do
  has_pact_with 'UserService' do
    mock_service :user_service do
      port 1234
    end
  end
end