class BlogPostsController < ApplicationController
  def show
    blog_post = BlogPost.find params[:id]
    render json: blog_post.as_json(methods: [:user])
  end
end
