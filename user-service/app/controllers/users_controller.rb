class UsersController < ApplicationController

  before_action :set_headers

  def set_headers
    response.headers['Content-Type'] = 'application/json'
  end

  def show
    user = User.find(params[:id])
    render json: user.to_json

    # render json: user.to_json(:except => [:email])
  end
end
