require_relative 'provider_state_for_blog'
require 'pact/provider/rspec'

Pact.service_provider 'UserService' do  
  honours_pacts_from_pact_broker do
    # Base URL of pact broker is mandatory
    # basic auth username/password and token are optional parameters
    pact_broker_base_url ENV['PACT_BROKER_BASE_URL']
    verbose true # Set this to true to see the HTTP requests and responses logged    
    app_version "main"
    app_version_branch "main"
    publish_verification_results true
  end
end